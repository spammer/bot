(function (i, s, o, g, r, a, m) {
  i['GoogleAnalyticsObject'] = r;
  i[r] = i[r] || function () { (i[r].q = i[r].q || []).push(arguments) },
  i[r].l = 1 * new Date();
  a = s.createElement(o),
  m = s.getElementsByTagName(o)[0];
  a.async = 1;
  a.src = g; m.parentNode.insertBefore(a, m) })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga_ca');
ga_ca('create', 'UA-1499181-43', 'auto', 'cad', { 'sampleRate': 0.1 });
ga_ca('cad.send', 'pageview');
  function familyFriendlyHandler(wid, cb) {
  var cad = new Image(1, 1); var cnet = new Image(1, 1); var ccom = new Image(1, 1);
  if (cb !== undefined && cb.checked) {
    cad.src = 'https://api.content.ad/family-friendly/enable';
    cnet.src = 'https://api.content-ad.net/family-friendly/enable';
    ccom.src = 'https://app.content-ad.com/FamilyFriendly/Enable?widgetId=' + wid;
  } else {
    cad.src = 'https://api.content.ad/family-friendly/disable';
    cnet.src = 'https://api.content-ad.net/family-friendly/disable';
    ccom.src = 'https://app.content-ad.com/FamilyFriendly/Disable';
  }
}
