const cheerio = require('cheerio');
const fs = require('fs');
const glob = require('glob');
const htmlmin = require('html-minifier').minify;

const jQueryCDN = '<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>';

glob(`${__dirname}/../adtypes/**/*.html`, (err, files) => {
  files.forEach((f) => {
    const code = fs.readFileSync(f, 'utf8');
    const $adHtml = cheerio.load(code);

    $adHtml.root().find('script').remove();
    $adHtml('body').append(jQueryCDN);
    fs.writeFileSync(f, htmlmin($adHtml.html(), {
      collapseWhitespace: true,
      minifyCSS: true
    }));
  });
});
