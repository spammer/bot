const bot = require('./lib/bot');
const keypress = require('./lib/keypress');
const Options = require('./lib/Options');
const ProxyCtrl = require('./lib/ProxyController');

if (!process.env.NODE_ENV) {
  console.error('run "node dev.js" or "node production.js" instead!');
  process.exit();
}
Options.server = `http://localhost:808${process.env.NODE_ENV === 'production' ? 0 : 1}`;
Options.routerServer = 'https://94.100.96.66:8789';
// process.on('SIGINT') doesn't allow async functions
keypress.onExit = async () => {
  await ProxyCtrl.stopAllDockers(); // TODO: use EventEmitter with promises
  ProxyCtrl.stopSelenium();
  if (process.env.NODE_ENV === 'production') await bot.closeBrowsers();
};
ProxyCtrl.runSelenium();
for (let i = 1; i <= (process.argv[2] || 10); i++) {
  bot.init(i);
}
