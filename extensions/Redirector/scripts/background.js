/* globals chrome */
// const setHeader = (details, hName, hValue) => {
//   const h = details.requestHeaders.find(h => h.name === hName);

//   if (h) h.value = hValue;
//   else {
//     details.requestHeaders.push({
//       name: hName,
//       value: hValue
//     });
//   }
// };

chrome.privacy.network.webRTCIPHandlingPolicy.set({
  value: chrome.privacy.IPHandlingPolicy.DISABLE_NON_PROXIED_UDP
});
// TODO: this shit doesn't work at all, probably because 'extraHeaders' were missing
// chrome.webRequest.onBeforeSendHeaders.addListener((details) => { // TODO: trying without async
//   if (~details.url.indexOf('{{POSTLINK}}')) {
//     setHeader(details, 'Referer', '{{REFERER}}');
//     setHeader(details, 'Accept-Language', '{{LANGUAGE}}');
//     // console.log(details.requestHeaders); // headers are changed here
//     // TODO: check with Wireshark, because according to Chrome DevTools network is not modified
//     // TODO: then check is and why Requestly extension is working:
//     // https://chrome.google.com/webstore/detail/requestly-redirect-url-mo/mdnleldcmiljblolnjhpnblkcekpdkpa
//   }
//   return {
//     requestHeaders: details.requestHeaders
//   };
// }, {
//   urls: [
//     '{{POSTLINK}}'
//   ]
// }, [
//   'blocking',
//   'requestHeaders'
//   'extraHeaders'
// ]);
let _urls = [];
const _cookies = [];
const listenerArgs = [d => _urls.push(d), { urls: ['<all_urls>'] }];

chrome.webRequest.onErrorOccurred.addListener(...listenerArgs);
chrome.webRequest.onCompleted.addListener(...listenerArgs);

const tasks = {
  getCookies: (params, cb) => cb(_cookies),
  getPages: (pages, cb) => cb(_urls.filter(d => pages.some(p => new RegExp(p).test(d.url)))),
  resetURLs: (params, cb) => {
    _urls = [];
    cb(true);
  }
};

// TODO: see why chrome.cookies.getAll doesn't work on production
// while it's ok on development
chrome.cookies.onChanged.addListener((info) => {
  if (info.removed || info.cookie.session) return;

  const cookie = _cookies.find(c => c.name === info.cookie.name);

  if (cookie) Object.assign(cookie, info.cookie);
  else _cookies.push(info.cookie);
});

chrome.runtime.onMessage.addListener((msg, sender, sendResponse) => {
  tasks[msg.task](msg.params, (results) => {
    sendResponse({
      tabId: sender.tab.id,
      results
    });
  });
  return true;
});

JSON.parse('{{COOKIES}}').forEach((c) => {
  chrome.cookies.set({
    url: '{{WEBSITE}}/*',
    name: c.name,
    value: c.value,
    domain: c.domain,
    path: c.path,
    secure: c.secure,
    httpOnly: c.httpOnly,
    sameSite: c.sameSite,
    expirationDate: c.expirationDate,
    storeId: c.storeId
  });
});
