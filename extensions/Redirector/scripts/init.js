/* globals document, window */
Object.defineProperty(document, 'referrer', {
  get: () => '{{REFERER}}'
});

Object.defineProperty(window.navigator, 'language', {
  get: () => '{{LANGUAGE}}'
});
