/* globals chrome, window, document */
const script = document.createElement('script');

script.onload = function onloadInit() {
  this.remove();
};
script.setAttribute('redirectorid', chrome.runtime.id);
script.src = chrome.runtime.getURL('scripts/init.js');
(document.head || document.documentElement).prepend(script);

// background -> webpage (disfunctional)
chrome.runtime.onMessage.addListener((msg) => {
  window.postMessage(msg, '*');
});

// webpage -> background -> webpage
window.addEventListener('message', (e) => {
  if (!e.data.task) return;
  chrome.runtime.sendMessage(e.data, (v) => {
    if (!v) {
      console.error(chrome.runtime.lastError);
      v = {};
    }
    window.postMessage({
      client: e.data.id,
      results: v.results,
      tabId: v.tabId
    }, '*');
  });
}, false);
