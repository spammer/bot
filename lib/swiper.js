const _ = require('lodash');
const bezier = require('./random-bezier');

exports.browser = null;

exports.getSwipeBox = (options) => {
  const metrics = options.desiredCapabilities.chromeOptions.mobileEmulation.deviceMetrics;
  const w = Math.round(metrics.width * 0.5);
  const h = Math.round(metrics.height * 0.7);
  const x0 = Math.round((metrics.width - w) / 2);
  const x1 = x0 + w;
  const y0 = Math.round((metrics.height - h) / 2);
  const y1 = y0 + h;
  const thirdH = Math.round(h / 3);
  const tenthH = Math.round(h / 10);
  const innerHr1 = y0 + thirdH;
  const innerHr2 = innerHr1 + thirdH;
  const xDev = 10;

  return {
    swipe: async (direction, ms = 300) => {
      const isUp = direction === 'up';
      const startX = _.random(x0, x1);
      const startY = _.random(isUp ? innerHr1 : y0, isUp ? y1 : innerHr2);
      const opts = {
        X0: startX,
        Y0: startY,
        Xf: _.random(startX - xDev, startX + xDev),
        Yf: _.random(isUp ? y0 : startY + tenthH, isUp ? startY - tenthH : y1),
        O: `T${_.random(ms - 200, ms + 200)} OT0 OB0 OL${xDev} OR${xDev} P5-15 S${isUp ? 'R' : ''}`,
        onStart: this.browser.touchDown,
        onMove: this.browser.touchMove,
        onEnd: this.browser.touchUp
      };

      await bezier(opts);
    }
  };
};
