// cSpell:ignore KHTML, repost
const _ = require('lodash');
const fs = require('fs-extra');
const { request } = require('./promises');

class Options {
  constructor(commands) {
    this.c = commands;
    if (process.env.NODE_ENV === 'production') {
      this.drawerMode = 'OnlyMain';
      this.consoleSelectedTab = 'console';
    } else {
      this.drawerMode = 'Both';
      this.consoleSelectedTab = 'network';
    }
    this.wdio = {
      desiredCapabilities: {
        browserName: 'chrome',
        pageLoadStrategy: 'none',
        // timeouts: { pageLoad: 0 },
        chromeOptions: {
          // https://peter.sh/experiments/chromium-command-line-switches/
          args: [
            'auto-open-devtools-for-tabs',
            // 'user-agent=Mozilla/5.0 (iPhone; CPU iPhone OS 11_3 like Mac OS X)
            //   AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1',
            // 'window-size=375,667',
            // `user-data-dir=${__dirname}/../tmp/user-data`,
            // 'start-maximized',
            // 'use-mobile-user-agent'
          ],
          excludeSwitches: [
            // 'disable-background-networking',
            // 'disable-client-side-phishing-detection',
            // 'disable-default-apps',
            // 'disable-hang-monitor',
            // 'disable-popup-blocking',
            // 'disable-prompt-on-repost',
            // 'disable-sync',
            // 'disable-web-resources',
            // 'enable-automation'
            // 'enable-logging',
            // 'force-fieldtrials=SiteIsolationExtensions/Control',
            // 'ignore-certificate-errors',
            // 'log-level=0',
            // 'metrics-recording-only',
            // 'no-first-run',
            // 'password-store=basic',
            // 'remote-debugging-port=0',
            // 'test-type=webdriver',
            // 'use-mock-keychain',
            // 'flag-switches-begin',
            // 'flag-switches-end'
          ],

          // https://github.com/ChromeDevTools/devtools-frontend/blob/master/front_end/emulated_devices/module.json
          // mobileEmulation: {
          //   // deviceName: 'Galaxy S5',
          //   deviceMetrics: {
          //     width: 360,
          //     height: 640,
          //     pixelRatio: 3,
          //     touch: true
          //   },
          //   userAgent: 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_3 like Mac OS X)
          // AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1'
          // },
          prefs: {
            browser: {
              app_window_placement: {
                DevToolsApp: {
                  always_on_top: false,
                  bottom: 600,
                  left: 930, // 930
                  maximized: false,
                  right: 1630,
                  top: 0
                }
              },
              // chrome has transparent left, right and bottom borders with 4px width
              window_placement: {
                bottom: 800, // this is dynamic
                left: 0,
                maximized: false,
                right: 1683,
                top: 0
              }
            },
            devtools: {
              preferences: {
                'Inspector.drawerSplitViewState': `{"horizontal":{"size":200,"showMode":"${this.drawerMode}"}}`,
                'InspectorView.splitViewState': '{"vertical":{"size":500},"horizontal":{"size":450}}',
                autoRevealInNavigator: 'false',
                blockedURLs: '[]',
                cacheDisabled: 'true',
                'console.sidebar.width': '{"vertical":{"size":0,"showMode":"OnlyMain"}}',
                'console.sidebarSelectedFilter': '"message"',
                consoleGroupSimilar: 'true',
                consoleShowSettingsToolbar: 'false',
                cssSourceMapsEnabled: 'false',
                currentDockState: '"right"',
                // download and upload = KiB * 128
                customNetworkConditions: '[{"title":"very slow","download":1920,"upload":1280,"latency":100}]',
                disableAsyncStackTraces: 'true',
                domWordWrap: 'false',
                'drawer-view-closeableTabs': '{"network.config":true}',
                'drawer-view-selectedTab': '"network.config"',
                'drawer-view-tabOrder': '{"console-view":10,"changes.changes":20,"network.config":30}',
                'emulation.autoAdjustScale': 'false',
                'emulation.deviceScale': '1',
                'emulation.showDeviceMode': 'false',
                'help.show-release-note': 'false',
                highlightNodeOnHoverInOverlay: 'false',
                inlineVariableValues: 'false',
                jsSourceMapsEnabled: 'false',
                // networkBlockedPatterns: '[{"enabled":true,"url":"osd.js"}]',
                networkBlockedURLs: '[]',
                'panel-selectedTab': `"${this.consoleSelectedTab}"`,
                textEditorAutoDetectIndent: 'false',
                textEditorAutocompletion: 'false',
                textEditorBracketMatching: 'false'
              }
            },
            extensions: {
              ui: {
                developer_mode: true
              }
            }
          }
        }
      },
      deprecationWarnings: false
    };
    if (this.c.test.connection !== 'local') {
      const port = this.c.test.connection === 'Shtip' ? 0 : this.c.lane;

      this.browserCmdId = `proxy-server=socks5://127.0.0.1:2200${port}`;
      this.wdio.desiredCapabilities.chromeOptions.args
        .push('host-resolver-rules=MAP * ~NOTFOUND , EXCLUDE 127.0.0.1', this.browserCmdId);
    }
    // TODO: used only for testing, but this should be calculated precisely
    this.deviceSize = {
      width: 1200,
      height: 800
    };
  }

  async setEmulator() {
    const defaultEmulator = 'iPhone 6/7/8 Plus';
    const deviceList = await Options.getDeviceList();
    const chromePrefs = this.wdio.desiredCapabilities.chromeOptions.prefs;
    const devtoolsPrefs = chromePrefs.devtools.preferences;
    let foundEmulator = _.find(deviceList, ['title', this.c.emulator]);

    if (!foundEmulator) {
      this.c.logError(`warning: emulator '${this.c.emulator}' not found! UA: "${this.c.UserAgent}". Using default "${defaultEmulator}"`);
      foundEmulator = _.find(deviceList, ['title', defaultEmulator]);
    }
    Object.assign(foundEmulator, {
      title: `-${this.c.emulator}-`,
      'user-agent': this.c.UserAgent,
      'show-by-default': true
    });
    this.deviceSize = {
      width: foundEmulator.screen.vertical.width,
      height: foundEmulator.screen.vertical.height,
      pixelRatio: foundEmulator.screen.vertical['device-pixel-ratio'],
      touch: true
    };
    this.wdio.desiredCapabilities.chromeOptions.mobileEmulation = {
      deviceMetrics: this.deviceSize,
      userAgent: this.c.UserAgent
    };
    devtoolsPrefs.customEmulatedDeviceList = JSON.stringify([foundEmulator]);
    devtoolsPrefs['emulation.deviceModeValue'] = JSON.stringify({
      device: foundEmulator.title,
      orientation: 'vertical',
      mode: 'default'
    });
    devtoolsPrefs['emulation.showDeviceMode'] = 'true';

    // 190 = from browser.top to deviceScreen.top
    // 30 = 4 (invisible bottom border) + 2 (bottom border) + 44 (extra space)
    chromePrefs.browser.window_placement.bottom = 190 + foundEmulator.screen.vertical.height + 50;
  }

  static async getDeviceList() {
    const body = fs.readFileSync('standardEmulatedDeviceList.json', 'utf8');
    // TODO: download on every month and diff
    // const body = await request('https://raw.githubusercontent.com/ChromeDevTools/devtools-frontend/master/front_end/emulated_devices/module.json');
    const list = _.map(JSON.parse(body).extensions, 'device');
    list.push(...JSON.parse(fs.readFileSync('newEmulatedDeviceList.json')));
    return list;
  }

  async restoreSession() {
    const v = this.c.prevSession ? JSON.parse(await request({
      url: `${Options.server}/api/boss/session`,
      qs: { cmdId: this.c.prevSession }
    })) : {};

    if (!v.cookies || !v.cookies.length) return;
    this.c.cookies = v.cookies;
    this.c.UserAgent = v.UserAgent;
    this.c.emulator = v.emulator;
  }

  useRedirector() {
    if (!this.c.referer) return;

    const src = 'extensions/Redirector';
    const dest = `dist/Redirector/${this.c.lane}`;
    const replacePairs = {
      '{{COOKIES}}': JSON.stringify(this.c.cookies),
      '{{LANGUAGE}}': 'en-US',
      '{{POSTLINK}}': this.c.posts[0],
      '{{REFERER}}': this.c.referer,
      '{{WEBSITE}}': this.c.website
    };
    const fsReplace = (file) => {
      let txt = fs.readFileSync(`${src}/${file}`, 'utf8');

      _.forEach(replacePairs, (v, k) => {
        txt = txt.replace(new RegExp(k, 'g'), v || '');
      });
      fs.writeFileSync(`${dest}/${file}`, txt);
    };

    fs.ensureDirSync(dest);
    fs.emptyDirSync(dest);
    fs.copySync(src, dest);
    fsReplace('manifest.json');
    fsReplace('scripts/background.js');
    fsReplace('scripts/init.js');
    this.wdio.desiredCapabilities.chromeOptions.args
      .push(`load-extension=${__dirname}/../dist/Redirector/${this.c.lane}`);
  }

  async saveHistory() {
    await request({
      method: 'POST',
      url: `${Options.server}/api/boss/history?lane=${this.c.lane}`,
      json: this.c
    });
  }
}

module.exports = Options;
