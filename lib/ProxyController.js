// cSpell:ignore Dselenium, Dwebdriver
const fs = require('fs');
const { spawn, spawnSync } = require('child_process');
const Options = require('./Options');
const { request } = require('./promises');

class ProxyCtrl {
  constructor(commands, options) {
    this.c = commands;
    this.options = options;
  }

  static runSelenium() {
    const seleniumStream = fs.createWriteStream('tmp/selenium.log');

    ProxyCtrl.selenium = spawn('java', [
      // -Dorg.seleniumhq.jetty9.util.log.class=org.seleniumhq.jetty9.util.log.JavaUtilLog \
      // -Dwebdriver.chrome.logfile=tmp/chromedriver.log \
      '-Dselenium.LOGGER.level=WARNING',
      '-Dwebdriver.chrome.driver=chromedriver.exe',
      '-jar', 'selenium-server-standalone-3.12.0.jar'
    ]);
    ProxyCtrl.selenium.stderr.pipe(seleniumStream);
    ProxyCtrl.selenium.stdout.pipe(seleniumStream);
  }

  static stopSelenium() {
    ProxyCtrl.selenium.kill();
    spawnSync('taskkill', ['/F', '/IM', 'chromedriver.exe']);
  }

  async startVPN() {
    if (this.c.test.connection) return true;
    try {
      await request({
        method: 'POST',
        url: `${Options.routerServer}/start?lane=${this.c.lane}`,
        agentOptions: ProxyCtrl.agentOptions,
        json: {
          lane: this.c.lane,
          zip: Buffer.from(this.c.VPN.config.data).toString('base64')
        }
      });
    } catch (ex) {
      this.c.logError(`startVPN: dns: ${this.c.VPN.dns}, ${ex.message}`);
      return false;
    }
    return true;
  }

  async stopVPN() {
    if (this.c.test.connection) return;
    try {
      await request({
        url: `${Options.routerServer}/end?lane=${this.c.lane}`,
        agentOptions: ProxyCtrl.agentOptions
      });
    } catch (ex) {
      this.c.logError(`stopVPN: ${ex.stack}`);
    }
  }

  static async stopAllDockers() {
    // if (process.env.NODE_ENV !== 'production') return;
    try {
      await request({
        url: `${Options.routerServer}/endall`,
        agentOptions: ProxyCtrl.agentOptions
      });
    } catch (ex) {
      this.c.logError(`stopAllDockers: ${ex.stack}`);
    }
  }
}
ProxyCtrl.agentOptions = {
  ca: fs.readFileSync('certificates/ca.crt'),
  cert: fs.readFileSync('certificates/cert.crt'),
  key: fs.readFileSync('certificates/cert.key'),
  rejectUnauthorized: false
};

module.exports = ProxyCtrl;
