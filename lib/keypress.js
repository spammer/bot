const sleep = require('util').promisify(setTimeout);

const {
  exit, stdin, stdout, stderr
} = process;
const m = {};

stdin.setRawMode(true);
stdin.resume();
stdin.setEncoding('utf8');
stdin.on('data', async (key) => {
  if (key === '\u0003') { // ctrl-c
    if (m.onExit) {
      await m.onExit();
    }
    exit();
    return;
  }
  if (key === '\r') {
    key = '\n';
  }
  stdout.write(key);
});
if (process.env.NODE_ENV === 'production') {
  const logFN = require('path').normalize(`${__dirname}/../tmp/master.log`);
  const logStream = require('fs').createWriteStream(logFN);

  console.log(`logs redirected to ${logFN}`);
  stdout.write = stderr.write = logStream.write.bind(logStream);
}
process.on('uncaughtException', (err) => {
  console.error((err && err.stack) ? err.stack : err);
});
process.cleanExit = async () => {
  await sleep(200);
  await m.onExit();
  exit();
};

module.exports = m;
