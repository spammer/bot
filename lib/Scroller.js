/* eslint-env node, browser */
const _ = require('lodash');
const moment = require('moment');
const PQueue = require('p-queue');
const { request, sleep } = require('./promises');
const DOMCtrl = require('./DOM');
const Options = require('./Options');

class Scroller {
  constructor(domCtrl, adCtrl) {
    this.adCtrl = adCtrl;
    this.browser = domCtrl.browser;
    this.c = adCtrl.data.commands;
    this.domCtrl = domCtrl;
    this.exitAt = moment(this.c.exitAt);
    this.c.linkIdx = 0;
    this.scrollStretch = 2.7;
    this.touchCoords = {
      x: [Math.round(domCtrl.deviceSize.width * 0.15)],
      y: [Math.round(domCtrl.deviceSize.height * 0.15)]
    };
    this.touchCoords.x.push(domCtrl.deviceSize.width - this.touchCoords.x[0]);
    this.touchCoords.y.push(domCtrl.deviceSize.height - this.touchCoords.y[0]);
  }

  async roam(postIdx) {
    const exitTime = this.c.ad.postIdx === postIdx ? this.c.ad.timeToSearch : 1;
    const clickFn = link => async () => this.clickLink(link);
    let chosenLink;

    if (this.c.test.skipRoam) return;
    for (;;) {
      await this.adCtrl.getAdsOffsets();
      await this.adCtrl.saveVisibleAds();
      if (!this.hasLinksToClick() && this.exitAt - moment() < exitTime) break;
      if (this.linkTime()) {
        await this.readLinkOffset();
        if (!chosenLink) chosenLink = this.c.links[this.c.linkIdx];
        await this.scrollTo(chosenLink);
        await Scroller.queue.add(clickFn(chosenLink));
        this.c.linkIdx++;
        if (await this.hasErrors(chosenLink.url)) break;
        chosenLink = null;
        continue;
      }
      await this.randomScroll();
      await sleep(_.random(...this.c.scroll.wait));
    }
  }

  async hasErrors(page) {
    try {
      await this.browser.execute(() => window.postMessage({ task: 'resetURLs' }, '*'));
      if (await this.hasErrorsInPages(['\\/(\\?seed=.+)?$'])) return true;
      await this.domCtrl.markCommand();
      await this.readScrollHeight();
      await this.domCtrl.setWindowTitle(page);
      return await this.hasErrorsInPages(['google-analytics.+collect', 'pixel\\.gif\\?']);
    } catch (ex) {
      this.c.logError(`Scroller.hasErrors: ${ex.message}, dns: ${this.c.VPN.dns}, linkIdx: ${this.c.linkIdx}, timeout: ${this.newScriptTimeout}`);
      return false;
    }
  }

  async hasErrorsInPages(pages) {
    let error;

    await this.extendScriptTimeout();
    try {
      error = (await this.browser.executeAsync((pages, timeout, end) => {
        // console.log(new Date().toISOString(), 'timeout:', timeout);
        const timer = setInterval(() => {
          window.postMessage({
            id: 'hasErrorsInPages',
            task: 'getPages',
            params: pages
          }, '*');
        }, 200);
        let listener, connectionTrials = 0;
        const finish = (error) => {
          clearInterval(timer);
          window.removeEventListener('message', listener, false);
          end(error);
        };
        const findError = () => {
          if (!document || !document.body) return;
          let div = document.querySelector('.error-code[jscontent]');
          if (div) return div.textContent;
          if (document.body.children.length < 4 && /^H\d/i.test(document.body.children[0].tagName)) {
            return document.body.children[0].textContent;
          }
          div = document.querySelector('.grecaptcha-badge');
          if (div) return 'reCAPTCHA';
          // if (document.readyState === 'loading') {
          //   document.addEventListener('DOMContentLoaded', () => {
          //     done(findError());
          //   });
          // } else done(findError());
        };
        let c = 0;

        listener = (e) => {
          if (e.data.client !== 'hasErrorsInPages') {
            if (++connectionTrials > 20) {
              console.log('overrun', e.data.client);
              finish();
            }
            return;
          }
          if (!e.data.results.length) {
            if (!(++c % 10)) {
              const error = findError();
              if (error) finish(error);
            }
            connectionTrials = 0;
            return;
          }
          finish();
        };
        window.addEventListener('message', listener, false);
      }, pages, moment.utc(this.newScriptTimeout).format('mm:ss'))).value;
      if (error) {
        this.c.logError(`Scroller.hasErrorsInPages: '${error}' vpn: ${this.c.VPN.dns}`);
        if (/captcha/i.test(error)) {
          await request({
            url: `${Options.server}/api/boss/report-vpn`,
            qs: {
              bossId: this.c.boss,
              dns: this.c.VPN.dns,
              error
            }
          });
        }
      }
    } catch (ex) {
      this.c.logError(`Scroller.hasErrorsInPages: ${ex.message}, dns: ${this.c.VPN.dns},`
        + ` linkIdx: ${this.c.linkIdx}, scriptTimeout: ${this.newScriptTimeout}, pages: ${pages}`);
      this.domCtrl.printScreen(`hasErrorsInPages-${this.c._id}-link${this.c.linkIdx}`);
      error = true;
    }
    await this.browser.timeouts(this.defaultTimeouts);
    return error;
  }

  async readScrollHeight() {
    this.scrollHeight = (await this.browser.executeAsync((done) => {
      const timer = setInterval(() => {
        if (!document || !document.documentElement || !document.body) return;
        clearInterval(timer);
        done(document.documentElement.scrollHeight - document.documentElement.clientHeight);
      }, 200);
    })).value;
  }

  async extendScriptTimeout() {
    if (this.c.next) {
      this.newScriptTimeout = moment(this.c.next.visitAt).subtract(50, 'seconds').diff(moment());
      this.newScriptTimeout = Math.round(this.newScriptTimeout / (this.c.links.length - this.c.linkIdx + 1) / 3);
    } else this.newScriptTimeout = 0;
    this.newScriptTimeout = 30000 + (this.newScriptTimeout > 0 ? this.newScriptTimeout : 0);
    this.defaultTimeouts = (await this.browser.timeouts()).value;
    await this.browser.timeouts(Object.assign({}, this.defaultTimeouts, {
      script: this.newScriptTimeout
    }));
  }

  async waitForLink() {
    const link = this.c.links[this.c.linkIdx];

    link.selId = (await this.browser.executeAsync((selectors, done) => {
      const timer = setInterval(() => {
        const links = document.querySelectorAll(selectors);

        if (links && links.length) {
          const idx = Math.floor(Math.random() * links.length);

          clearInterval(timer);
          links[idx].setAttribute('data-chosenLink', idx);
          done(idx);
        }
      }, 200);
    }, link.selectors)).value;
    link.selector = `[data-chosenLink="${link.selId}"]`;

    return link;
  }

  async readLinkOffset() {
    try {
      const link = await this.waitForLink();
      const offsets = await this.domCtrl.getOffsets([link.selector]);

      Object.assign(link, offsets[0]);
    } catch (ex) {
      this.c.logError(`readLinkOffset: ${ex.stack}`);
    }
  }

  hasLinksToClick() {
    return this.c.links && this.c.links.length > this.c.linkIdx;
  }

  linkTime() {
    return this.hasLinksToClick()
      && (moment() - this.c.sessionStartedAt + 5000) > this.c.links[this.c.linkIdx].time;
  }

  async clickLink(link) {
    const size = await this.domCtrl.getSize(null, link.selector);
    const clickPos = DOMCtrl.getClickPos(null, size.new.w, size.new.h);
    const location = await this.domCtrl.getLocation(link.selector);

    link.url = await this.browser.getAttribute(link.selector, 'href');
    await this.domCtrl.click(size, clickPos, location, this.c.ad.pressDuration);
    link.clickedAt = moment();
  }

  async saveLinkImage(selector) {
    const fnScreenshot = `tmp/${this.c._id}.png`;
    const adSize = await this.browser.getElementSize(selector);
    const adLocation = await this.domCtrl.getLocation(selector);

    await this.browser.saveScreenshot(fnScreenshot);
    await require('easyimage').crop({
      src: fnScreenshot,
      dst: fnScreenshot,
      cropWidth: adSize.width,
      cropHeight: adSize.height,
      x: adLocation.rel.x,
      y: adLocation.rel.y
    });
  }

  async scrollTo(chosenLink) {
    const x = _.random(...this.touchCoords.x);
    const y1 = this.c.scroll.minPoints * Math.sign(chosenLink.offScreen); // TODO: test minPoints
    const y2 = Math.round(chosenLink.offScreen * this.scrollStretch + y1);

    Scroller.assert('scrollTo', { y2, offScreen: chosenLink.offScreen });
    await this.browser.touchScroll(null, x, y2);
    if (!await this.browser.isVisibleWithinViewport(chosenLink.selector)) {
      await this.browser.touchScroll((await this.browser.$(chosenLink.selector)).value.ELEMENT, 0, 0);
    }
  }

  static assert(fn, args) {
    if (Object.values(args).filter(a => !_.isNumber(a)).length) {
      const err = new Error(`${fn}: ${JSON.stringify(args)}`);
      err.code = 'ESCROLL';
      throw err;
    }
  }

  // async setBrowserIP() {
  //   if (this.c.test.connection) return '';

  //   const browserIP = (await this.browser.selectorExecute('#clientIP', inputs => inputs[0].value));

  //   if (this.c.IP !== browserIP) {
  //     this.c.logError(`:) this could happen too: ${this.c.IP} != ${browserIP}`);
  //     this.c.IP += ` (IN_BROWSER: ${browserIP})`;
  //   }
  // }

  async lookToClick(postIdx) {
    if (this.c.ad.postIdx !== postIdx) return;
    if (!this.adCtrl.allSlots.length) {
      this.c.logError('Not a single ad found');
      return;
    }

    const clickFn = slot => async () => this.adCtrl.clickAd(slot);
    let chosenSlot, slots;

    for (;;) {
      await this.adCtrl.getAdsOffsets();
      await this.adCtrl.saveVisibleAds();
      if (!chosenSlot) {
        // TODO: add this.adCtrl.getAvailableSlot();
        // when at least plenty of patterns will become available
        slots = this.adCtrl.savedSlots.length
          ? this.adCtrl.savedSlots
          : this.adCtrl.allSlots;
        chosenSlot = this.adCtrl.getTestSlot(slots) || _.sample(slots);
      }
      if (this.c.test.afterAdClick
        || this.exitAt - moment() < this.c.ad.emergencyClickTime) {
        for (const slot of this.adCtrl.allSlots) {
          // browser.scroll doesn't work when mobileEmulation is used
          await this.browser.touchScroll((await this.browser
            .$(slot.adSelector)).value.ELEMENT, 0, 0);
          await this.adCtrl.saveAd(slot);
          if (slot.size) {
            await Scroller.queue.add(clickFn(chosenSlot));
            break;
          }
        }
        this.c.logError('Refused clicking on any ads');
        break;
      }
      if (_.find(this.adCtrl.visibleSlots, ['clickSlotId', chosenSlot.clickSlotId])) {
        if (!chosenSlot.size) {
          chosenSlot = null;
          continue;
        }
        await Scroller.queue.add(clickFn(chosenSlot));
        await this.roamAfterAdClick();
        break;
      }
      this.scrollTo(chosenSlot);
      await sleep(_.random(...this.c.ad.searchWait));
    }
  }

  async roamAfterAdClick() {
    let isWindowChanged = false;
    const isSameWindow = async () => {
      isWindowChanged = false;
      return !isWindowChanged;
      // TODO: needs help from extension
      // if (isWindowChanged) return false;
      // if (this.tabId !== await this.browser.getCurrentTabId()
      //   || this.windowTitle !== await this.browser.getTitle()) return !(isWindowChanged = true);
      // return true;
    };

    this.scrollHeight = null;
    for (;;) {
      if (this.exitAt - moment() < this.c.ad.searchWait[0]) break;
      if (await isSameWindow()) {
        await sleep(2000);
        continue;
      }
      // TODO: this is unreachable until find a way
      // to focus different tabs from the same window
      // (check multiremote function)
      if (!this.scrollHeight) await this.readScrollHeight(true);
      await this.randomScroll();
      await sleep(_.random(...this.c.scroll.wait));
    }
  }

  async randomScroll() {
    if (!this.scrollHeight) return;

    const fifthHeight = this.scrollHeight / 5;
    const scrollTop = (await this.browser.execute(() => window.pageYOffset
      || document.documentElement.scrollTop)).value;
    let direction;

    if (scrollTop < fifthHeight) direction = 1;
    else if (scrollTop > this.scrollHeight - fifthHeight) direction = -1;
    else direction = _.sample([-1, 1]);

    const x = _.random(...this.touchCoords.x);
    const y1 = this.c.scroll.minPoints * direction;
    const scroll = direction > 0 ? this.scrollHeight - scrollTop : -scrollTop;
    const y2 = Math.round(scroll * this.scrollStretch + y1);
    const y = _.random(y1, y2);
    // console.log('roam', wait, this.scrollHeight, scrollTop, y);
    // Y * 0.3633 + Y / 248

    Scroller.assert('randomScroll', {
      y1, y2, scrollStretch: this.scrollStretch, scrollTop
    });
    await this.browser.touchScroll(null, x, this.c.test.fullScroll ? y2 : y);
  }
}
Scroller.queue = new PQueue({ concurrency: 1 });

module.exports = Scroller;
