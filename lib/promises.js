const processWindows = require('node-process-windows');
const { promisify } = require('util');

exports.getProcesses = promisify(processWindows.getProcesses);

exports.request = require('request-promise');

exports.sleep = async ms => promisify(setTimeout)(+ms > 0 ? +ms : 0);
