/* eslint-env node, browser */
// cSpell:ignore bayandin
const _ = require('lodash');
const { execSync } = require('child_process');
const robot = require('robotjs');
const screenshot = require('screenshot-desktop');

const { sleep } = require('./promises');

class DOMCtrl {
  constructor(browser, options) {
    const chromeOpts = options.wdio.desiredCapabilities.chromeOptions;

    this.browser = browser;
    this.browserCmdId = options.browserCmdId;
    this.browserSize = chromeOpts.prefs.browser.window_placement;
    this.c = options.c;
    this.deviceSize = options.deviceSize;
    this.devtoolsWidth = JSON.parse(chromeOpts.prefs
      .devtools.preferences['InspectorView.splitViewState']).vertical.size;
  }

  async saveSession() {
    try {
      const v = (await this.browser.executeAsync((end) => {
        window.postMessage({ id: 'saveSession', task: 'getCookies' }, '*');
        window.addEventListener('message', (e) => {
          if (e.data.client !== 'saveSession') return;
          end({ results: e.data.results });
        }, false);
      })).value;

      if (v.error) {
        this.c.logError(`warning from extension: ${v.error}. Skipped saving cookies.`);
        return;
      }
      this.c.cookies = v.results;
    } catch (ex) {
      this.c.logError(`saveSession: ${ex.message}`);
    }
  }

  /**
   * Return Y offset for every queried element.
   * offScreen is 0 for visible elements.
   *
   * @param {Array[String]} selectors Array of CSS selectors
   * @returns {Array[Object]} [{ offScreen: Number, visible: Boolean }, ...]
   */
  async getOffsets(selectors) {
    const script = (input) => {
      const v = [];

      JSON.parse(input).forEach((sel) => {
        const el = document.querySelector(sel);

        if (!el) return;

        const elClip = el.getBoundingClientRect();
        const o = {
          visible: (elClip.top > 0) && (elClip.top < (window.innerHeight - elClip.height))
        };

        o.offScreen = o.visible ? 0 : Math.round(elClip.top);
        v.push(o);
      });

      return JSON.stringify(v);
    };
    return JSON.parse((await this.browser.execute(script, JSON.stringify(selectors))).value);
  }

  static getClickCoords(size, clickPos, location) {
    let [x, y] = clickPos.split(':').map(Number);

    x *= size.new.w / size.original.w;
    x = _.sample([x - 1, x, x + 1]);
    y *= size.new.h / size.original.h;

    return {
      xAbs: Math.round(x + location.abs.x),
      yAbs: Math.round(y + location.abs.y),
      xRel: Math.round(x + location.rel.x),
      yRel: Math.round(y + location.rel.y)
    };
  }

  async getLocation(selector) {
    return {
      abs: await this.browser.getLocation(selector),
      rel: await this.browser.getLocationInView(selector)
    };
  }

  async getSize(strOriginalSize, strNewSize) {
    let original, newSize;

    if (strOriginalSize) original = strOriginalSize.split('x').map(Number);
    if (/^\d+x\d+$/.test(strNewSize)) {
      newSize = strNewSize.split('x').map(Number);
    } else {
      const v = await this.browser.getElementSize(strNewSize);

      newSize = [v.width, v.height];
    }
    if (!original) original = newSize;

    return {
      original: { w: original[0], h: original[1] },
      new: { w: newSize[0], h: newSize[1] }
    };
  }

  static getClickPos(clicksObj, width, height) {
    const hasClickPositions = clicksObj && clicksObj.positions;

    // if (!hasClickPositions) {
    //   this.c.logError(`Ad on '${slot.clickSlotId}'
    // with pattern '${slot.pattern || ''}' has empty clicks.positions!`);
    // }
    return hasClickPositions
      ? _.sample(clicksObj.positions.split(','))
      : [width, height].map(s => _.random(1, s)).join(':');
  }

  async robotMove(offsetX, offsetY) {
    const { right } = this.browserSize;
    const { width } = this.deviceSize;

    const x = Math.round(offsetX + (right - width - this.devtoolsWidth) / 2);
    const y = offsetY + 191;

    // x = _.random(x - 3, x + 3);
    // y = _.random(y - 2, y + 2);
    await this.focusBrowser();
    await robot.moveMouse(x, y);
  }

  async focusBrowser() {
    const execOutput = execSync(`wmic process where name="chrome.exe" get CommandLine, ProcessID | grep ${this.browserCmdId} | grep -oP "\\s{2,}\\d+" | grep -oP "\\d+"`);
    const pid = +execOutput.toString('utf8').trim();

    execSync(`windows-console-app.exe --focus ${pid}`, { cwd: 'win-pid' });
  }

  async printScreen(filename) {
    await this.focusBrowser();
    await screenshot({ filename: `tmp/screenshots/${filename}.png` });
  }

  async click(size, clickPos, location, pressDuration) {
    const coords = DOMCtrl.getClickCoords(size, clickPos, location);

    try {
      // await this.saveLinkImage(size, location);
      await this.robotMove(coords.xRel, coords.yRel);
      await this.browser.touchDown(coords.xAbs, coords.yAbs);
      await sleep(_.random(...pressDuration));
      await this.browser.touchUp(coords.xAbs, coords.yAbs);
      await robot.mouseClick();
    } catch (ex) {
      this.c.logError(`click: ${JSON.stringify(coords)} ${JSON.stringify(size)}\n${ex.stack}`);
      await this.browser.saveScreenshot(`tmp/screenshots/click-${this.c._id}-link${this.c.linkIdx}.png`);
    }
    // TODO: search for actions implementation in selenium
    // Change github.com/bayandin/chromedriver/dispatch_touch_event.js and compile
    // touchClick(ad.value.ELEMENT); // not able to send x, y. Also requires hard browser.scroll
    // buttonDown(); // hangs same as buttonPress()
  }

  async setWindowTitle(url) {
    await this.browser.execute((newTitle) => {
      document.title = newTitle;
    }, `${url} (bl=${this.c.lane})`);
  }

  async markCommand() {
    await this.browser.execute(cmdId => window.commandId = cmdId, this.c._id);
  }

  async saveLinkImage(size, location) {
    const fnScreenshot = `tmp/screenshots/${this.c._id}-link${this.c.linkIdx}.png`;

    await this.browser.saveScreenshot(fnScreenshot);
    await require('easyimage').crop({
      src: fnScreenshot,
      dst: fnScreenshot,
      cropWidth: size.new.w,
      cropHeight: size.new.h,
      x: location.rel.x,
      y: location.rel.y
    });
  }
}

module.exports = DOMCtrl;
