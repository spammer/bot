/* eslint-env node, browser */
const _ = require('lodash');
const cheerio = require('cheerio');
const { crop } = require('easyimage');
const fs = require('fs');
const { minify } = require('html-minifier');
const DOMCtrl = require('./DOM');
const Options = require('./Options');
const { request, sleep } = require('./promises');

class AdCtrl {
  constructor(domCtrl, data) {
    const contentAdHtmlGetter = async adSelector => this.browser.selectorExecute(adSelector,
      ads => ads[0].parentNode.parentNode.parentNode.parentNode.outerHTML);

    this.advertiers = [{
      advertiser: 'Google Ads',
      adsSelector: 'ins[data-ad-slot]',
      idMatch: 'data-ad-slot="?(\\d+)',
      templateAdSelector: '[data-ad-slot="{{adID}}"]',
      getHtml: async adSelector => this.browser
        .$(`${adSelector} iframe`)
        .then(iframe => this.browser.frame(iframe.value))
        .$('[id^=google_ads]')
        .then(iframe => this.browser.frame(iframe.value))
        .getHTML('html')
    }, {
      // manySmall
      advertiser: 'Content.AD',
      adsSelector: 'div[id^=ac_]:not([class]) .ac_container',
      idMatch: 'wid=(\\d+)',
      templateAdSelector: '#ac_{{adID}} .ac_container:nth-child({{adIndex}})',
      getHtml: contentAdHtmlGetter
    }, {
      // oneBig
      advertiser: 'Content.AD',
      adsSelector: '.ads1.cols1.rows1 .ac_container',
      idMatch: 'wid=(\\d+)',
      templateAdSelector: '#ac_{{adID}}.ads1.cols1.rows1 .ac_container',
      getHtml: contentAdHtmlGetter
    }];
    this.browser = domCtrl.browser;
    this.data = data;
    this.c = data.commands;
    this.dom = domCtrl;
    this.savedSlots = [];
    // this.getNextFolder = (source = 'adtypes') => `unknown${fs.readdirSync(source)
    //   .filter(n => fs.lstatSync(join(source, n)).isDirectory())
    //   .map(n => Number((/^unknown(\d*)$/.exec(n) || [0, 0])[1]))
    //   .sort((a, b) => a - b)
    //   .pop() + 1}`;
  }

  async getAdSlots() {
    const script = (advertiers) => {
      const retVal = [], indexes = {};
      const fnSetPositions = aInfo => (ad) => {
        const posObj = {
          advertiser: aInfo.advertiser,
          clickSlotId: ad.outerHTML.match(new RegExp(aInfo.idMatch))[1]
        };

        posObj.adsSelector = aInfo.adsSelector;
        posObj.index = indexes[posObj.advertiser] = (indexes[posObj.advertiser] + 1) || 1;
        posObj.adSelector = aInfo.templateAdSelector.replace('{{adID}}', posObj.clickSlotId).replace('{{adIndex}}', posObj.index);
        retVal.push(posObj);
      };

      JSON.parse(advertiers).forEach((a) => {
        document.querySelectorAll(a.adsSelector).forEach(fnSetPositions(a));
      });

      return JSON.stringify(retVal);
    };
    const input = JSON.stringify(this.advertiers);

    this.allSlots = [];
    try {
      this.allSlots = JSON.parse((await this.browser.execute(script, input)).value);
      this.allSlots.forEach((slot) => {
        slot.getHtml = _.find(this.advertiers, ['adsSelector', slot.adsSelector]).getHtml;
      });
    } catch (ex) {
      this.c.logError(`getAdSlots: ${ex}`);
    }

    if (this.c.test.clickSlot) {
      const slot = AdCtrl.getTestSlot(this.allSlots);
      await this.browser.touchScroll((await this.browser.$(slot.adSelector)).value.ELEMENT, 0, 0);
      await sleep(2000); // TODO: (low priority) make smarter wait on ad image load
    }

    return this.allSlots;
  }

  async getAdsOffsets() {
    this.visibleSlots = [];
    try {
      const offsets = await this.dom.getOffsets(this.allSlots.map(s => s.adSelector));

      this.allSlots.forEach((s, i) => {
        Object.assign(s, offsets[i]);
        if (offsets[i].visible) this.visibleSlots.push(s);
      });
    } catch (ex) {
      this.c.logError(`getAdsOffsets: ${ex}`);
    }
  }

  async saveVisibleAds() {
    for (const s of this.visibleSlots) {
      await this.saveAd(s);
    }
  }

  async saveAd(slot) {
    if (_.find(this.savedSlots, ['clickSlotId', slot.clickSlotId])) return;

    const adHeight = await this.browser.getElementSize(slot.adSelector, 'height');

    if (!adHeight) {
      // this.c.logError(`saveAd: ${slot.advertiser} - ${slot.clickSlotId} height is 0!`);
      return;
    }

    const $adHtml = await this.getAdCode(slot);

    if (!$adHtml('body').text().trim().length) {
      // this.c.logError(`saveAd: ${slot.advertier} - ${slot.clickSlotId} code is empty!`);
      console.error('code is empty', $adHtml.html());
      return;
    }

    const adp = this.findMatchedPattern($adHtml);
    let code = $adHtml.html();
    let codeError;

    try {
      code = minify(code, {
        collapseWhitespace: true,
        minifyCSS: true
      });
    } catch (ex) {
      codeError = true;
    }
    Object.assign(slot, {
      pattern: adp.name,
      patternSize: adp.adSize,
      clicks: adp.clicks,
      code: Buffer.from(code)
    });
    await this.saveAdImage(slot);
    adp._id = await this.saveNewPattern(slot);
    if (codeError) this.c.logError(`HTML code on newly saved adpattern (${adp._id}) breaks the minifier`);
    this.savedSlots.push(slot);
  }

  async getAdCode(slot) {
    const jQueryCDN = '<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>';
    const html = AdCtrl.fixHtmlParsingErrors(await slot.getHtml(slot.adSelector));
    const $adHtml = cheerio.load(html);

    await this.browser.frameParent().frameParent();
    $adHtml.root().find('script').remove();
    $adHtml('body').append(jQueryCDN);

    return $adHtml;
  }

  static fixHtmlParsingErrors(html) {
    return html
      .replace(/ ([^"]+)"="" /g, ' $1="" '); // content.ad fuckery
  }

  findMatchedPattern($adHtml) {
    let matchedPatterns = [];

    for (const p of this.data.ads.patterns) {
      let matches = 0;

      for (const s of p.selPatterns) {
        if ($adHtml(s.selector).length === s.length) matches++;
      }
      if (matches && matches === p.selPatterns.length) matchedPatterns.push(p);
    }
    if (matchedPatterns.length !== 1) {
      if (matchedPatterns.length > 1) {
        matchedPatterns = _.map(matchedPatterns, p => _.omit(p, 'clicks'));
        this.c.logError(`Overlapping matches: ${JSON.stringify(matchedPatterns, null, 2)}`);
      }
      return {};
    }
    return matchedPatterns[0];
  }

  async saveAdImage(slot) {
    const fnScreenshot = `tmp/${(Math.random() + 1).toString(36).substring(2)}.png`;
    const adSize = await this.browser.getElementSize(slot.adSelector);
    const adLocation = await this.dom.getLocation(slot.adSelector);

    await this.browser.saveScreenshot(fnScreenshot);
    await crop({
      src: fnScreenshot,
      dst: fnScreenshot,
      cropWidth: adSize.width,
      cropHeight: adSize.height,
      x: adLocation.rel.x,
      y: adLocation.rel.y
    });
    // `data:image/png;base64,${fs.readFileSync(fnScreenshot, 'base64')}`
    slot.image = fs.readFileSync(fnScreenshot);
    slot.size = `${adSize.width}x${adSize.height}`;
    fs.unlinkSync(fnScreenshot);
  }

  async saveNewPattern(slot) {
    if (this.c.test.skipAdSave) return;

    return request({
      method: 'POST',
      url: `${Options.server}/api/boss/ad-variant`,
      json: {
        advertiser: slot.advertiser,
        adSize: slot.size,
        image: slot.image,
        code: slot.code
      }
    });
  }

  getAvailableSlot() {
    if (!this.savedSlots.length) {
      this.c.logError('No recorded ad patterns!');
      return null;
    }

    const slot = _.filter(this.savedSlots, 'clicks.positions');

    if (!slot.length) {
      this.c.logError('Can not find single ad pattern with filled click positions.');
      return null;
    }

    return _.sample(slot);
  }

  async clickAd(slot) {
    if (!slot.size) {
      this.c.logError('Not a single ad saved before click');
    }

    const size = await this.dom.getSize(slot.patternSize, slot.size || slot.adSelector);
    const clickPos = DOMCtrl.getClickPos(slot.cicks, size.new.w, size.new.h);
    const location = await this.dom.getLocation(slot.adSelector);

    await this.dom.click(size, clickPos, location, this.c.ad.pressDuration);
    this.c.ad.clickPos = clickPos;
  }

  getTestSlot(slots) {
    if (!this.c.test.clickSlot) return false;
    return _.find(slots, ['clickSlotId', this.c.test.clickSlot]);
  }
}

module.exports = AdCtrl;
