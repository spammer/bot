const crypto = require('crypto');
const moment = require('moment');
const { Socks5 } = require('socks5');
const wdio = require('webdriverio');

const AdCtrl = require('./AdController');
const DOMCtrl = require('./DOM');
const Options = require('./Options');
const { getProcesses, request, sleep } = require('./promises');
const ProxyCtrl = require('./ProxyController');
const Scroller = require('./Scroller');

class Bot {
  constructor(data) {
    this.data = data;
    this.c = data.commands;
    this.c.next = data.nextCommands;
    this.visitAt = moment(this.c.visitAt);
  }

  async run() {
    // const timelog = `b${this.c.lane}-l${loopIdx}`;
    await sleep(this.visitAt - moment() - 60000);
    this.options = new Options(this.c);
    this.proxyCtrl = new ProxyCtrl(this.c);
    this.socks5 = new Socks5(this.c);
    // console.time(`onConnect-${timelog}`);
    if (!await this.proxyCtrl.startVPN()) {
      await this.options.saveHistory();
      return;
    }
    await this.socks5.connect();
    if (!await this.socks5.setIP()) {
      await this.options.saveHistory();
      this.socks5.disconnect();
      await this.proxyCtrl.stopVPN();
      return;
    }
    // console.timeEnd(`onConnect-${timelog}`); // at least 3 s
    // console.time(`onInit-${timelog}`);
    await this.options.restoreSession();
    this.options.useRedirector();
    await this.options.setEmulator();
    // mongo --eval 'db.commands.remove({})' gd
    this.browser = wdio.remote(this.options.wdio);
    this.domCtrl = new DOMCtrl(this.browser, this.options);
    this.adCtrl = new AdCtrl(this.domCtrl, this.data);
    this.scroller = new Scroller(this.domCtrl, this.adCtrl);
    // console.timeEnd(`onInit-${timelog}`); // less than 100ms
    this.website = new URL(this.c.website).hostname; // TODO: should be many websites instead of one
    await sleep(this.visitAt - moment() - 10000);
    await this.browser.init().on('error', this.c.logError).catch(this.c.logError);
    await sleep(this.visitAt - moment());
    // console.time(`onRun-${timelog}`);
    for (let i = 0; i < this.c.posts.length; i++) {
      await this.browser.url(`${this.c.posts[i]}?seed=${crypto.randomBytes(8).toString('hex')}`);
      // await sleep(moment(0).add(15, 'hours'));
      if (await this.scroller.hasErrors(this.c.posts[0])) continue;
      this.c.sessionStartedAt = moment();
      this.scroller.exitAt.add(this.c.sessionStartedAt - this.visitAt);
      // console.log((await browser.execute('return document.documentElement.lang')).value);
      // console.log((await browser.execute('return navigator.language')).value);
      await this.adCtrl.getAdSlots();
      await this.scroller.roam(i);
      await this.scroller.lookToClick(i);
      await sleep(this.scroller.exitAt - moment());
      this.c.sessionDuration = moment() - this.c.sessionStartedAt;
    }
    // console.timeEnd(`onRun-${timelog}`);
    await this.finish();
  }

  async finish() {
    try {
      // await sleep(moment(0).add(15, 'hours'));
      await this.domCtrl.saveSession();
      // await scroller.setBrowserIP(); // not needed when #clientIP is gone
      // console.time(`onEnd-${timelog}`);
      await this.options.saveHistory();
      // console.log('browser end'); await sleep(moment(0).add(15, 'hours'));
      await this.browser.end();
      this.socks5.disconnect();
      await this.proxyCtrl.stopVPN();
      // console.timeEnd(`onEnd-${timelog}`);
    } catch (ex) {
      this.c.logError(`bot.finish: ${ex.message}`);
    }
  }
}

exports.init = async (lane) => {
  const minutes = [1, 5, 15, 30, 60, 120];
  let data, wait, tried = minutes.slice(), bot, msg;

  for (let i = 0; ; i++) {
    try {
      data = JSON.parse(await request(`${Options.server}/api/boss/commander?lane=${lane}`));
      if (!data.commands) throw new Error(`No more commands for bot ${lane}.`);
      data.commands.logError = this.logError.bind(data.commands);
    } catch (ex) {
      if (tried.length > 0) wait = tried.shift();
      console.error(`${ex.message}. Sleep for ${wait} minutes and then try again...`);
      await sleep(wait * 60 * 1000);
      continue;
    }
    bot = new Bot(data);
    try {
      await bot.run(i);
    } catch (ex) {
      msg = ex.stack;
      if (ex.code === 'ESCROLL'
        || /script timeout/.test(ex.message)) msg = ex.message;
      bot.c.logError(msg);
      try {
        await bot.domCtrl.printScreen(`unknown-${bot.c._id}`);
      } catch (ex) {
        bot.c.logError(ex.message);
      }
      await bot.finish();
    }
    tried = minutes.slice();
    if (i >= process.argv[3] - 1) break;
  }
  console.log(moment().format('MMMDD HH:mm:ss'), 'bot', lane, 'finish');
};

exports.logError = function logError(...error) {
  this.errorMsg = (this.errorMsg ? `${this.errorMsg}\n` : '') + error;
  console.error(`db.commands.findOne(ObjectId('${this._id}'))`,
    moment().format('MMMDD HH:mm:ss'), 'bot', this.lane, `error:\n\t"${error}"`);
};

exports.closeBrowsers = async () => {
  (await getProcesses()).forEach((p) => {
    if (new RegExp(`(data:,|${this.website}|\\(bl=\\d\\))`, 'i').test(p.mainWindowTitle)) {
      process.kill(p.pid);
    }
  });
  // Object.values(Socks5.forks).map(o => o.disconnect()); // TODO: to be or not to be
};
