const { Tick, timers } = require('exectimer');

module.exports = class Time {
  constructor(label) {
    this.label = `timer${label}`;
    this.tick = new Tick(this.label);
  }

  start() {
    this.tick.start();
    return this.label;
  }

  stop() {
    let time = timers[this.label];

    this.tick.stop();
    if (!time) return 0;
    time = Math.round(time.duration() / 1000000) / 1000;
    delete timers[this.label];
    return time;
  }
};
