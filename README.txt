Required before `npm install`:
* Python 2.7
* https://github.com/nodejs/node-gyp#on-windows
* https://www.imagemagick.org/script/download.php#windows
* JDK SE 1.8 x64
